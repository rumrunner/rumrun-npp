
--[[
Factorio knows how to present a recipe in the crafting menu, if there is a
single product, eg, an iron gear is the product of iron plates.  However, this
mod will have multiple products when downgrading (uncrafting),
so we supply the info normally inferred by the item: icon, subgroup, order.
]]

local function npp_recipe(n_x_m, suffix, ingredients, products)
	local full_name = n_x_m .. (suffix == "" and "" or "-" .. suffix)
	-- down/up grade recipes come after: "b" is ordered before "d" and "u".
	local order_prefix = suffix == "" and "b" or string.sub(suffix, 1, 1)
	local order_name = order_prefix .. n_x_m
	return {
		type = "recipe",
		name = npp_recipe_name(full_name),
		results = (products ~= nil and products) or {{npp_item_name(n_x_m), 1}},
		ingredients = (ingredients ~= nil and ingredients) or {{npp_item_name(n_x_m), 1}},
		energy_required = 15,
		enabled = false,
		icon = npp_icon_path(n_x_m, suffix),
		icon_size = 64,
		subgroup = "energy",
		order = npp_order(order_name),
	}
end

local function npp_downgrade_recipe(n_x_m, ingredients, products)
	return npp_recipe(n_x_m, "downgrade", ingredients, products)
end
local function npp_upgrade_recipe(n_x_m, ingredients, products)
	return npp_recipe(n_x_m, "upgrade", ingredients, products)
end

data:extend({
	npp_downgrade_recipe("1x1", nil, {
		{"storage-tank",       6},
		{"steam-turbine",      7},
		{"heat-pipe",          5},
		{"heat-exchanger",     4},
		{"nuclear-reactor",    1},
	}),
	npp_recipe("1x1", "", {
		{"storage-tank",       6},
		{"steam-turbine",      7},
		{"heat-pipe",          5},
		{"heat-exchanger",     4},
		{"nuclear-reactor",    1},
	}),
	npp_downgrade_recipe("2x1", nil, {
		{npp_item_name("1x1"), 2},
		{"steam-turbine",      14},
		{"heat-pipe",          34},
		{"heat-exchanger",     8},
	}),
	npp_upgrade_recipe("2x1", {
		{npp_item_name("1x1"), 2},
		{"steam-turbine",      14},
		{"heat-pipe",          34},
		{"heat-exchanger",     8},
	}),
	npp_recipe("2x1", "", {
		{"storage-tank",       12},
		{"steam-turbine",      28},
		{"heat-pipe",          44},
		{"heat-exchanger",     16},
		{"nuclear-reactor",    2},
	}),
	npp_downgrade_recipe("2x2", nil, {
		{npp_item_name("2x1"), 2},
		{"steam-turbine",      28},
		{"heat-pipe",          8},
		{"heat-exchanger",     16},
	}),
	npp_upgrade_recipe("2x2", {
		{npp_item_name("2x1"), 2},
		{"steam-turbine",      28},
		{"heat-pipe",          8},
		{"heat-exchanger",     16},
	}),
	npp_recipe("2x2", "", {
		{"storage-tank",       24},
		{"steam-turbine",      84},
		{"heat-pipe",          96},
		{"heat-exchanger",     48},
		{"nuclear-reactor",    4},
	}),
	npp_downgrade_recipe("2x3", nil, {
		{npp_item_name("2x2"), 1},
		{npp_item_name("2x1"), 1},
		{"steam-turbine",      26},
		{"heat-pipe",          18},
		{"heat-exchanger",     16},
	}),
	npp_upgrade_recipe("2x3", {
		{npp_item_name("2x2"), 1},
		{npp_item_name("2x1"), 1},
		{"steam-turbine",      26},
		{"heat-pipe",          18},
		{"heat-exchanger",     16},
	}),
	npp_recipe("2x3", "", {
		{"storage-tank",       36},
		{"steam-turbine",      138},
		{"heat-pipe",          158},
		{"heat-exchanger",     80},
		{"nuclear-reactor",    6},
	}),
	npp_downgrade_recipe("2x4", nil, {
		{npp_item_name("2x3"), 1},
		{npp_item_name("2x1"), 1},
		{"steam-turbine",      28},
		{"heat-pipe",          98},
		{"heat-exchanger",     16},
	}),
	npp_upgrade_recipe("2x4", {
		{npp_item_name("2x3"), 1},
		{npp_item_name("2x1"), 1},
		{"steam-turbine",      28},
		{"heat-pipe",          98},
		{"heat-exchanger",     16},
	}),
	npp_recipe("2x4", "", {
		{"storage-tank",       48},
		{"steam-turbine",      194},
		{"heat-pipe",          300},
		{"heat-exchanger",     112},
		{"nuclear-reactor",    8},
	}),
	npp_downgrade_recipe("2x5", nil, {
		{npp_item_name("2x4"), 1},
		{npp_item_name("2x1"), 1},
		{"steam-turbine",      26},
		{"heat-pipe",          4},
		{"heat-exchanger",     16},
	}),
	npp_upgrade_recipe("2x5", {
		{npp_item_name("2x4"), 1},
		{npp_item_name("2x1"), 1},
		{"steam-turbine",      26},
		{"heat-pipe",          4},
		{"heat-exchanger",     16},
	}),
	npp_recipe("2x5", "", {
		{"storage-tank",       60},
		{"steam-turbine",      248},
		{"heat-pipe",          348},
		{"heat-exchanger",     144},
		{"nuclear-reactor",    10},
	}),
	npp_downgrade_recipe("2x6", nil, {
		{npp_item_name("2x5"), 1},
		{npp_item_name("2x1"), 1},
		{"steam-turbine",      30},
		{"heat-pipe",          140},
		{"heat-exchanger",     16},
	}),
	npp_upgrade_recipe("2x6", {
		{npp_item_name("2x5"), 1},
		{npp_item_name("2x1"), 1},
		{"steam-turbine",      30},
		{"heat-pipe",          140},
		{"heat-exchanger",     16},
	}),
	npp_recipe("2x6", "", {
		{"storage-tank",       72},
		{"steam-turbine",      306},
		{"heat-pipe",          532},
		{"heat-exchanger",     176},
		{"nuclear-reactor",    12},
	}),
})
