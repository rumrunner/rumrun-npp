
local function pic(n_x_m, orientation, width, height)
	return {
		filename = npp_graphic_path(n_x_m, orientation),
		width = width,
		height = height,
	}
end
local function pic4way(n_x_m, width, height)
	return {
		north = pic(n_x_m, "n", width, height),
		south = pic(n_x_m, "s", width, height),
		east = pic(n_x_m, "e", height, width),
		west = pic(n_x_m, "w", height, width),
	}
end
local function pic4way_identical(n_x_m, width, height)
	return {
		north = pic(n_x_m, "n", width, height),
		south = pic(n_x_m, "s", width, height),
		east = pic(n_x_m, "e", width, height),
		west = pic(n_x_m, "w", width, height),
	}
end
local function pic2way(n_x_m, width, height)
	return {
		north = pic(n_x_m, "h", width, height),
		south = pic(n_x_m, "h", width, height),
		east = pic(n_x_m, "v", height, width),
		west = pic(n_x_m, "v", height, width),
	}
end

local function npp_entity(n_x_m, power, effectivity, layers, position)
	local entity = {
		type = "burner-generator",
		name = npp_entity_name(n_x_m),
		icon = npp_icon_path(n_x_m),
		icon_size = 64,
		flags = {"placeable-neutral", "player-creation"},
		minable = {mining_time = 2, result = npp_item_name(n_x_m)},
		max_health = 100000,
		corpse = "big-remnants",
		burner = {
			fuel_category = "nuclear",
			effectivity = effectivity,
			fuel_inventory_size = 1,
			burnt_inventory_size = 1,
		},
		max_power_output = power,
		energy_source = {
			type = "electric",
			usage_priority = "secondary-output",
		},
		animation = layers,
	}
	for k,v in pairs(position) do
		entity[k] = v
	end
	return entity
end

data:extend({
	npp_entity("1x1", "40MW", 1, pic4way_identical("1x1", 566, 571), {
		-- 17x17
		collision_box = {{-8.7, -8.9}, {8.7, 8.9}},
		selection_box = {{-8.7, -8.9}, {8.7, 8.9}},
		drawing_box = {{-8.7, -8.9}, {8.7, 8.9}},
	}),
	npp_entity("2x1", "160MW", 2, pic4way("2x1", 921, 990), {
		-- 28x30
		collision_box = {{-14.01, -15.01}, {14.01, 15.01}},
		selection_box = {{-14.01, -15.01}, {14.01, 15.01}},
		drawing_box = {{-14.00, -15.0}, {14.0, 15.0}},
	}),
	npp_entity("2x2", "480MW", 3, pic2way("2x2", 1563, 1623), {
		-- 48x50
		collision_box = {{-24.01, -25.01}, {24.01, 25.01}},
		selection_box = {{-24.01, -25.01}, {24.01, 25.01}},
		drawing_box = {{-24.01, -25.01}, {24.01, 25.01}},
	}),
	npp_entity("2x3", "800MW", 3.33, pic2way("2x3", 2140, 1975), {
		-- 66x61
		collision_box = {{-33.01, -30.51}, {33.01, 30.51}},
		selection_box = {{-33.01, -30.51}, {33.01, 30.51}},
		drawing_box = {{-33.01, -30.51}, {33.01, 30.51}},
	}),
	npp_entity("2x4", "1120MW", 3.5, pic2way("2x4", 2428, 2266), {
		-- 75x70
		collision_box = {{-37.6, -35.1}, {37.6, 35.1}},
		selection_box = {{-37.6, -35.1}, {37.6, 35.1}},
		drawing_box = {{-37.6, -35.1}, {37.6, 35.1}},
	}),
	npp_entity("2x5", "1440MW", 3.6, pic2way("2x5", 2747, 2622), {
		-- 85x80
		collision_box = {{-42.6, -39.9}, {42.6, 39.9}},
		selection_box = {{-42.6, -39.9}, {42.6, 39.9}},
		drawing_box = {{-42.6, -39.9}, {42.6, 39.9}},
	}),
	npp_entity("2x6", "1760MW", 3.67, pic2way("2x6", 3037, 3096), {
		-- 94x96
		collision_box = {{-47.01, -48.01}, {47.01, 48.01}},
		selection_box = {{-47.01, -48.01}, {47.01, 48.01}},
		drawing_box = {{-47.01, -48.01}, {47.01, 48.01}},
	}),
})
