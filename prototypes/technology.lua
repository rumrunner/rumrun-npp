
local function unlock_npp_recipe(n_x_m)
	return {
		type = 'unlock-recipe',
		recipe = npp_recipe_name(n_x_m)
	}
end

data:extend({
	{
		type = "technology",
		name = npp_technology_name("base"),
		icon = npp_icon_path("technology", "base"),
		icon_size = 128,
		prerequisites = {"nuclear-power"},
		effects = {
			unlock_npp_recipe("1x1"),
			unlock_npp_recipe("2x1"),
			unlock_npp_recipe("2x2"),
			unlock_npp_recipe("2x3"),
			unlock_npp_recipe("2x4"),
			unlock_npp_recipe("2x5"),
			unlock_npp_recipe("2x6"),
		},
		unit = {
			count = 100,
			ingredients = {
				{"automation-science-pack", 1},
				{"logistic-science-pack", 1},
				{"chemical-science-pack", 1},
			},
			time = 60
		}
	},
	{
		type = "technology",
		name = npp_technology_name("updown"),
		icon = npp_icon_path("technology", "updown"),
		icon_size = 128,
		prerequisites = {npp_technology_name("base")},
		effects = {
			unlock_npp_recipe("1x1-downgrade"),
			unlock_npp_recipe("2x1-downgrade"),
			unlock_npp_recipe("2x2-downgrade"),
			unlock_npp_recipe("2x3-downgrade"),
			unlock_npp_recipe("2x4-downgrade"),
			unlock_npp_recipe("2x5-downgrade"),
			unlock_npp_recipe("2x6-downgrade"),
			unlock_npp_recipe("2x1-upgrade"),
			unlock_npp_recipe("2x2-upgrade"),
			unlock_npp_recipe("2x3-upgrade"),
			unlock_npp_recipe("2x4-upgrade"),
			unlock_npp_recipe("2x5-upgrade"),
			unlock_npp_recipe("2x6-upgrade"),
		},
		unit = {
			count = 50,
			ingredients = {
				{"automation-science-pack", 1},
				{"logistic-science-pack", 1},
				{"chemical-science-pack", 1},
			},
			time = 30
		}
	}
})