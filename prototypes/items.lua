
local function npp_item(n_x_m, stack_size)
	return {
		type = "item",
		name = npp_item_name(n_x_m),
		icon = npp_icon_path(n_x_m),
		icon_size = 64,
		place_result = npp_entity_name(n_x_m),
		subgroup = "energy",
		-- Is overwritten by recipe to sort it properly with up/down grade
		-- when crafting, but is still probably used for inventory sort.
		order = npp_order(n_x_m),
		stack_size = stack_size or 1,
	}
end

data:extend({
	npp_item("1x1"),
	npp_item("2x1"),
	npp_item("2x2"),
	npp_item("2x3"),
	npp_item("2x4"),
	npp_item("2x5"),
	npp_item("2x6"),
})
