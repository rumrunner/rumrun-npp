for _, force in pairs(game.forces) do
	local techs = force.technologies
	local recipes = force.recipes
	if techs["rumrun:npp-base-technology"].researched then
		recipes["rumrun:npp-2x6-recipe"].enabled = true
	end
	if techs["rumrun:npp-updown-technology"].researched then
		recipes["rumrun:npp-2x6-downgrade-recipe"].enabled = true
		recipes["rumrun:npp-2x6-upgrade-recipe"].enabled = true
	end
end