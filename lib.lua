
function npp_icon_path(name, suffix)
	suffix = ((suffix == nil or suffix == "") and "") or "-" .. suffix
	return "__rumrun-npp__/graphics/icons/"
		.. name .. "/icon-" .. name .. suffix .. ".png"
end

function npp_graphic_path(n_x_m, orientation)
	return "__rumrun-npp__/graphics/entity/"
		.. n_x_m .. "/" .. n_x_m .. "-" .. orientation .. ".png"
end

function npp_order(order_name)
	return "f[nuclear-energy]-e[rumrun-npp-" .. order_name .. "]"
end

local function npp_prototype_name(name, type_of_prototype)
	return "rumrun:npp-" .. name .. "-" .. type_of_prototype
end

function npp_entity_name(name)
	return npp_prototype_name(name, "entity")
end
function npp_item_name(name)
	return npp_prototype_name(name, "item")
end
function npp_recipe_name(name)
	return npp_prototype_name(name, "recipe")
end
function npp_technology_name(name)
	return npp_prototype_name(name, "technology")
end
