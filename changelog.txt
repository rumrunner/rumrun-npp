---------------------------------------------------------------------------------------------------
Version: 1.1.0
Date: 2022-02-09

  Major Features:
    - Added 2x6 power plant.
  Changes:
    - The recipes for upgrade/downgrade now come after the base recipes and are grouped together for readability.
  Bugfixes:
    - Efficiency value was slightly off for 2x5 so the seconds to consume fuel is now accurate (22 -> 20) and all were retested.
    - A failed condition resulted in the ingredient or product to be missing for upgrade/downgrade recipes, which allowed for free items.

---------------------------------------------------------------------------------------------------
Version: 1.0.0
Date: 2022-02-02

  Major Features:
    - Created compact power plant entities for 1x1, 2x1, 2x2, 2x3, and 2x5.
    - Recipes are unlocked by researching technology after nuclear-power.
    - Dismantle "downgrade" any power plant.
    - Combine "upgrade" smaller power plants to make larger ones.
    - Downgrade/upgrade recipes are unlocked by researching technology, in lieu of toggling them with settings.
  Changes:
    - Redesigned the existing 2x4 power plant.
